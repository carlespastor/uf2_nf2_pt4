/*
* Dependencias
*/
var gulp = require('gulp'),
  concat = require('gulp-concat'),
  uglify = require('gulp-uglify'),
  optimizar_img = require('gulp-tinypng-nokey'),
  prefixer = require('gulp-autoprefixer');

/*
* Configuración de la tarea 'demo'
*/
gulp.task('produccio', function () {
  gulp.src('js/source/*.js')
  .pipe(concat('todo.js'))
  .pipe(uglify())
  .pipe(gulp.dest('js/build/'))
});

gulp.task('prefixer', () =>
    gulp.src('src/*.css')
        .pipe(prefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('css'))
);

gulp.task('images', function () {
    gulp.src('src/*.png')
        .pipe(optimizar_img())
        .pipe(gulp.dest('dist'));
});
